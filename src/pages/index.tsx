import Image from "next/image";
import { Inter } from "next/font/google";
import Head from "next/head";
import { Seo } from "@/ui/components/seo/seo";
import { Typography } from "@/ui/design-system/typography/typography";
import { Button } from "@/ui/design-system/button/button";
import { Ri4kLine } from "react-icons/ri";
import { Spinner } from "@/ui/design-system/spinner/spinner";
import { Logo } from "@/ui/design-system/logo/logo";
import { Avatar } from "@/ui/design-system/avatar/avatar";
import { Container } from "@/ui/components/container/container";
import { Navigation } from "@/ui/components/navigation/navigation";
import { Footer } from "@/ui/components/footer/footer";
import { Layout } from "@/ui/components/layout/layout";
import { LandingPageView } from "@/ui/modules/landing-page/landing-page.view";
import { LandingPageContainer } from "@/ui/modules/landing-page/landing-page.container";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Seo title="Mon site en React" description="bienvenue sur mon site" />
      <Layout isDisplayBreadcrumbs={false}>
        <LandingPageContainer />
      </Layout>
    </>
  );
}
