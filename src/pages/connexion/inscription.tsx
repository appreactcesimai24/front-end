import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { RegisterContainer } from "@/ui/modules/authentication/register/register.container";

export default function Inscription() {
  return (
    <>
      <Seo title="Inscription" description="Inscription à mon site" />
      <Layout>
        <RegisterContainer />
      </Layout>
    </>
  );
}
