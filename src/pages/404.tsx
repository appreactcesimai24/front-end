import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";
import { LandingPageContainer } from "@/ui/modules/landing-page/landing-page.container";
import Image from "next/image";

export default function Custom404() {
  return (
    <>
      <Seo title="Mon site en React" description="bienvenue sur mon site" />
      <Layout>
        <div className="items-center mx-auto my-20 text-center">
          <Image
            src="/asset/images/404.png"
            alt="Image d'une 404"
            width={550}
            height={620}
            className=" mx-auto pb-12"
          />
          <Typography variant="h4" component="h4" className="mb-7">
            Tu t'es perdu ou quoi ?
          </Typography>
          <div className="items-center mx-auto">
            <Button baseUrl="/">Retour page d'accueil</Button>
          </div>
        </div>
      </Layout>
    </>
  );
}
