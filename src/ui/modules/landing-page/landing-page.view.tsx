import { CallToActionView } from "./call-to-action/call-to-action.view";
import { FeaturedView } from "./components/features/features.view";
import { HeroTopView } from "./components/hero-top.view";
import { HighlightsContainer } from "./components/highlights/highlights.container";
import { LastVideoContainer } from "./components/last-video/last-video.container";
import { LastVideoView } from "./components/last-video/last-video.view";

export const LandingPageView = () => {
  return (
    <>
      <HeroTopView />
      <FeaturedView />
      <LastVideoView />
      <HighlightsContainer />
      <CallToActionView />
    </>
  );
};
