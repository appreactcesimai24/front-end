import { Container } from "@/ui/components/container/container";

import { Button } from "@/ui/design-system/button/button";

import { LinkTypes } from "@/lib/link-type";

import Image from "next/image";
import { Typography } from "@/ui/design-system/typography/typography";

export const CallToActionView = () => {
  return (
    <div className="relative overflow-hidden bg-primary">
      <Container className="">
        <div className="relative z-10 max-w-2xl space-y-5">
          <Typography
            variant="h2"
            theme="white"
            component="div"
            weight="regular"
          >
            N’attend pas pour développer tes compétences...
          </Typography>

          <div>
            <Button variant="white" baseUrl="#" linkType={LinkTypes.EXTERNAL}>
              Formation dev
            </Button>
          </div>
        </div>

        <div className="">
          <Image
            src="/asset/svg/bomb.svg"
            alt="image d'une bombe"
            width={1410}
            height={1410}
            className="absolute -bottom-[470px] -right-[250px]"
          />
        </div>
      </Container>
    </div>
  );
};
