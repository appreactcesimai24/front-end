import { LinkTypes } from "@/lib/link-type";

import { Container } from "@/ui/components/container/container";

import { Button } from "@/ui/design-system/button/button";

import { Logo } from "@/ui/design-system/logo/logo";

import { Typography } from "@/ui/design-system/typography/typography";

import Image from "next/image";

import { RiPlayCircleLine } from "react-icons/ri";

export const LastVideoView = () => {
  return (
    <div className="bg-white">
      <Container className="py-16 text-center">
        <Typography variant="h2" component="h2" className="space-y-O">
          Formations React/next
        </Typography>

        <Typography variant="lead" component="h3" className="mb-3">
          Apprends à coder avec nous
        </Typography>

        <Typography
          variant="caption3"
          theme="gray"
          component="p"
          className="mt-5"
        >
          Si tu veux un CV plus sexy que ton ex, suis cette formation !
        </Typography>

        <a href="#" target="_blank">
          <div className="relative bg-gray-500 rounded h-[626px]">
            <div className="flex flex-col items-center justify-center gap-2 relative h-full bg-gray z-10 rounded opacity-0 hover:opacity-85 text-white animate">
              <RiPlayCircleLine size={80} />

              <Typography
                variant="caption1"
                theme="white"
                className="uppercase"
                weight="medium"
              >
                Voir la formation
              </Typography>
            </div>

            <Image
              fill
              src="asset/images/last-video.jpeg"
              alt="cours React et NextJS"
              className="object-cover object-center rounded"
            />
          </div>
        </a>
      </Container>
    </div>
  );
};
