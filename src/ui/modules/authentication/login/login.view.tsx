import { Box } from "@/ui/design-system/box/box";
import { Typography } from "@/ui/design-system/typography/typography";
import Link from "next/link";
import { RegisterForm } from "../register/register.form";
import Image from "next/image";
import { Container } from "@/ui/components/container/container";
import { LoginForm } from "./login.form";

export const LoginView = () => {
  return (
    <Container className="grid grid-cols-2 gap-20 mb-32">
      <div className="flex items-center">
        <div className="relative w-full h-[531px]">
          <Image fill src="/asset/images/login.jpg" alt="image de login" />
        </div>
      </div>

      <div className="flex items-center">
        <Box padding_y="py-5">
          <div className=" flex items-center justify-between">
            <Typography variant="h5" component="h1">
              Connexion
            </Typography>

            <div className="flex items-center gap-2">
              <Typography variant="caption4" component="span" theme="gray">
                Mot de passe oublié ?
              </Typography>

              <Typography variant="caption4" component="span" theme="primary">
                <Link href="/connexion">Inscription</Link>
              </Typography>
            </div>
          </div>

          <LoginForm />
        </Box>
      </div>
    </Container>
  );
};
