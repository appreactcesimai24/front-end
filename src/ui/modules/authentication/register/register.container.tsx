import { useForm, SubmitHandler } from "react-hook-form";

import { RegisterView } from "./register.view";
import { RegisterFormFielsType } from "@/types/forms";
import { useState } from "react";

import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "@/config/firebase-config";

export const RegisterContainer = () => {
  //const isLoading = false; //
  const [isLoading, setIsLoading] = useState<boolean>(false); // state on lui donne un type et une valeure par défaut a false

  const {
    handleSubmit,
    control,
    formState: { errors },
    register,
    setError,
    reset,
  } = useForm<RegisterFormFielsType>();

  const onSubmit: SubmitHandler<RegisterFormFielsType> = async (formData) => {
    setIsLoading(true);
    console.log("formData", formData);

    const { email, password } = formData; //destructuring pour pas avoir a faire formData.email

    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed up
        const user = userCredential.user;
        setIsLoading(false); //une fois que l'envoi est fait je repasse mon loading a false
        console.log(user); //on voit ce qu'on récupere
      })

      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        setIsLoading(false); //une fois que l'envoi est fait je repasse mon loading a false
        console.log(errorCode, errorMessage); //on voit ce qu'on récupere
      });
  };
  return (
    <>
      <RegisterView
        form={{
          errors,
          control,
          register,
          handleSubmit,
          onSubmit,
          isLoading,
        }}
      />
    </>
  );
};
