import { FormsType } from "@/types/forms";
import { Button } from "@/ui/design-system/button/button";
import { Input } from "@/ui/design-system/input/input";

interface Props {
  form: FormsType;
}

export const RegisterForm = ({ form }: Props) => {
  const { errors, control, register, handleSubmit, onSubmit, isLoading } = form;

  console.log("form", form);
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="space-y-5">
      <Input
        isLoading={isLoading}
        placeholder="adresse@gmail.com"
        type="email"
        register={register}
        errors={errors}
        errorMessage="ce champs doit être renseigné"
        id="email" // on renseigne la meme valeur que dans notre objet
      />

      <Input
        isLoading={isLoading}
        placeholder="mot-de-passe"
        type="password"
        register={register}
        errors={errors}
        errorMessage="ce champs doit être renseigné"
        id="password" // on renseigne la meme valeur que dans notre objet
      />

      <Input
        isLoading={isLoading}
        placeholder="d'ou viens-tu"
        type="text"
        register={register}
        errors={errors}
        errorMessage="ce champs doit être renseigné"
        id="how_did_know_us" // on renseigne la meme valeur que dans notre objet
      />

      <Button isLoading={isLoading} type="submit" fullWidth>
        envoyer
      </Button>
    </form>
  );
};
