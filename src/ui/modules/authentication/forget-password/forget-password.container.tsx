import { ForgetPasswordForm } from "./forget-password.form";

export const ForgetPasswordContainer = () => {
  return (
    <>
      <ForgetPasswordForm />
    </>
  );
};
