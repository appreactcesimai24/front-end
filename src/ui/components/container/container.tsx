import clsx from "clsx";

interface Props {
  children: React.ReactNode;
  className?: string;
}

export const Container = ({ children, className }: Props) => {
  return (
    <div className={clsx("max-w-6xl mx-auto space-y-5 py-10", className)}>
      {children}
    </div>
  );
};
