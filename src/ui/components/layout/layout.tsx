import clsx from "clsx";
import { Navigation } from "../navigation/navigation";
import { Footer } from "../footer/footer";
import { Button } from "@/ui/design-system/button/button";
import { RiArrowUpDoubleLine, RiArrowUpFill } from "react-icons/ri";
import ScrollToTop from "react-scroll-to-top";
import { Breadcrumbs } from "../breadcrumbs/breadcrumbs";

interface Props {
  children: React.ReactNode;
  isDisplayBreadcrumbs?: boolean;
}

export const Layout = ({ children, isDisplayBreadcrumbs = true }: Props) => {
  return (
    <>
      <Navigation />
      {isDisplayBreadcrumbs && <Breadcrumbs />}
      {children}
      <ScrollToTop
        smooth
        top={800}
        component={
          <RiArrowUpDoubleLine size={34} className=" text-primary mx-auto" />
        }
      />
      <Footer />
    </>
  );
};
