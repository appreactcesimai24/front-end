import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/router";
import { useMemo } from "react";

interface Props {
  href: string;
  children: React.ReactNode;
}

export const ActiveLink = ({ href, children }: Props) => {
  const router = useRouter();
  const isActive: boolean = useMemo(() => {
    return router.pathname === href; // on verifie si pathName = href
  }, [router.pathname, href]); //on ajoute un tableau de dépendance qui contient le chemin et le href, si une des 2 infos changes, useMemo va se reactiver sinon on fait rien

  return (
    <Link href={href} className={clsx(isActive && "text-primary font-medium")}>
      {children}
    </Link>
  );
};
