import { Button } from "@/ui/design-system/button/button";
import { footerSocialsLinks } from "./app-links";
import { RiFacebookBoxFill } from "react-icons/ri";

import { v4 as uuidv4 } from "uuid";
import clsx from "clsx";

interface Props {
  className?: string;
  theme?: "gray" | "accent" | "secondary";
}

export const SocialNetworkButtons = ({
  className = "flex items-center gap-2.5",
  theme = "accent",
}: Props) => {
  const icoList = footerSocialsLinks.map((socialNetwork) => (
    <Button
      key={uuidv4()}
      iconTheme={theme}
      variant="ico"
      icon={{
        icon: socialNetwork.icon ? socialNetwork.icon : RiFacebookBoxFill,
      }} //si socialNetwork icon existe => icone sinon, une icone par defaut
    />
  ));

  return <div className={clsx(className)}>{icoList}</div>;
};
