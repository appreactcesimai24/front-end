export interface RegisterFormFielsType {
  email: string;
  password: string;
  how_did_know_us: string;
}

export interface FormsType {
  errors: any;
  control: any;
  register: any;
  handleSubmit: any;
  onSubmit: any;
  isLoading: boolean;
}
