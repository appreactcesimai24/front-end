// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAqptTvpgJEjJfyU_WVNL3hva6fDSYt_4s",
  authDomain: "test17052024.firebaseapp.com",
  projectId: "test17052024",
  storageBucket: "test17052024.appspot.com",
  messagingSenderId: "301440289182",
  appId: "1:301440289182:web:c84263264e69742b75ae8f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app)